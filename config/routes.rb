# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :loan_officers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'conversations#index'
  resources :journeys, only: %i[index show]
end
