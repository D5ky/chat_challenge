#!/bin/bash

set -e

bundle install

rm -f tmp/pids/server.pid

bundle exec rake db:reset

/bin/bash
