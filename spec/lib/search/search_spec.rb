# frozen_string_literal: true

require 'rails_helper'

describe Search do
  let(:organization) { create :organization }
  let(:response) { described_class.new(organization, query).call }

  describe '#call' do
    let(:query) { 'User Name' }

    context 'when person name includes that value' do
      before { message }

      let(:person) { create :person, name: query }
      let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
      let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization }

      it 'displays conversations with that person' do
        expect(response.first.id).to be conversation.id
      end
    end

    context 'when person name does not include search value' do
      let(:query) { 'No_User_Name' }

      it 'displays nothing' do
        expect(response).to be_empty
      end
    end

    context 'with invalid params' do
      let(:search_query) { '%$#@' }

      it 'displays nothing' do
        expect(response).to be_empty
      end
    end
  end
end
