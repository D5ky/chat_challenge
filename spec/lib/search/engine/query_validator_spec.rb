# frozen_string_literal: true

require 'rails_helper'

describe Engine::QueryValidator do
  let(:response) { described_class.new(query).call }

  describe '#call' do
    let(:query) { 'User Name' }

    it 'when string is valid' do
      expect(response).to be_truthy
    end

    context 'when string length too long' do
      let(:query) { super() * 25 }

      it { expect(response).to be_falsey }
    end

    context 'with invalid char' do
      let(:query) { '%' }

      it { expect(response).to be_falsey }
    end
  end
end
