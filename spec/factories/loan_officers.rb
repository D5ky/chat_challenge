# frozen_string_literal: true

FactoryBot.define do
  factory :loan_officer, aliases: %i[robocop] do
    name { 'Alex Murphy' }
    email { 'amurphy@ocp.com' }
    password { '123456' }
    association :organization, factory: :organization
  end

  factory :officer_mcclain, class: 'LoanOfficer' do
    name { 'John McClain' }
    email { 'jmcclain@lapd.com' }
    password { '123456' }
    association :organization, factory: :organization
  end
end
