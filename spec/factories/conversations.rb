# frozen_string_literal: true

FactoryBot.define do
  factory :conversation do
    association :person, factory: :person
    association :organization, factory: :organization

    topic_name { 'this is a topic name' }

    transient do
      loan_officer { nil }
      exchanges { 0 }
    end

    after :create do |id, conversation|
      if conversation.exchanges > 0
        conversation.exchanges.times do
          create  :message,
                  :from_person,
                  author: conversation.person,
                  organization: conversation.organization,
                  conversation: id

          create  :message,
                  :from_loan_officer,
                  author: conversation.loan_officer,
                  organization: conversation.organization,
                  conversation: id
        end
      end
    end
  end
end
