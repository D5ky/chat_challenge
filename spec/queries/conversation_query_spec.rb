# frozen_string_literal: true

require 'rails_helper'

describe ConversationQuery do
  let(:organization) { create :organization }

  context 'when conversations with order by last fresh message' do
    before { conversations }

    let(:conversations) { create_list :conversation, 3, organization: organization }
    let(:last_message_at) { conversations.last.last_message_at }
    let(:response) { described_class.new(organization.conversations).order_last_massage_at_desc }

    it { expect(response.first.last_message_at).to eql last_message_at }
  end

  describe '#without_last_contact' do
    before { message }

    let(:response) { described_class.new(organization.conversations).without_last_contact }
    let(:person) { create :person }
    let(:conversation1) { create :conversation, exchanges: 0, person: person, organization: organization }
    let(:message) { create :message, conversation: conversation1, author_type: Person.to_s, author: person, organization: organization }

    it 'displays conversation' do
      expect(response.first.id).to be conversation1.id
    end
  end

  describe '#find_by_last_contact' do
    before { message }

    let(:name) { 'Loan Officer' }
    let(:response) { described_class.new(organization.conversations).find_by_last_contact(organization.id, name) }
    let(:loan_officer) { create :loan_officer, organization: organization, name: name }
    let(:conversation) { create :conversation, exchanges: 0, organization: organization }
    let(:message) { create :message, conversation: conversation, author_type: LoanOfficer.to_s, author: loan_officer, organization: organization }

    it 'displays conversation' do
      expect(response.first.id).to be conversation.id
    end
  end
end
