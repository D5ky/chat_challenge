# frozen_string_literal: true

require 'rails_helper'

describe ConversationsController, '#index' do
  let(:organization) { create :organization }
  let(:loan_officer) { create :loan_officer, organization: organization }
  let!(:conversations) { create_list :conversation, 3, organization: organization }

  context 'when logged in officer' do
    before { sign_in loan_officer }

    context 'when the organization has conversations' do
      before { get :index }

      it 'displays conversations' do
        conversations.each do |conversation|
          expect(response.body).to have_selector "#conversation-#{conversation.id}"
        end
      end
    end

    context 'when the organization does not have any conversations' do
      before { get :index }

      it 'displays conversations table' do
        expect(response.body).to have_selector '#conversations'
      end
    end

    context 'when another organization has conversations and the current organization does not' do
      let!(:organization2) { create :organization }
      let!(:conversations) { create_list :conversation, 3, organization: organization2 }

      before { get :index }

      it 'does not display conversations belonging another organization' do
        conversations.each do |conversation|
          expect(response.body).not_to have_selector "#conversation-#{conversation.id}"
        end
      end
    end
  end

  context 'when no officer is logged in' do
    before { get :index }

    it 'redirects to authentication path' do
      expect(response).to redirect_to new_loan_officer_session_path
    end
  end

  context 'when goes to the conversations page' do
    context 'when conversations present' do
      before do
        sign_in(loan_officer)
        get(:index)
      end

      it 'each conversation has a topic_name field' do
        conversations.each do |conversation|
          expect(response.body).to have_selector ".topic_name-#{conversation.id}"
        end
      end

      it 'each conversation has a person_city field' do
        conversations.each do |conversation|
          expect(response.body).to have_selector ".person_city-#{conversation.id}"
        end
      end

      it 'each conversation has a last_message_at field' do
        conversations.each do |conversation|
          expect(response.body).to have_selector ".last_message_at-#{conversation.id}"
        end
      end

      context 'when displays button "Show conversations without Last Contact"' do
        it { expect(response.body).to have_selector '#conversations_without_last_contact' }
      end

      context 'when click button "Show conversations without Last Contact"' do
        let(:response) { get :index, params: { without_last_contact: true } }

        context 'when conversations without Last Contact present' do
          before { message }

          let(:person) { create :person }
          let(:conversation1) { create :conversation, exchanges: 0, person: person, organization: organization }
          let(:message) { create :message, conversation: conversation1, author_type: Person.to_s, author: person, organization: organization }
          let(:conversation2) { create :conversation, person: person, organization: organization }

          it 'displays conversations' do
            expect(response.body).to have_selector "#conversation-#{conversation1.id}"
          end

          it 'does not display conversations with last contact' do
            expect(response.body).not_to have_selector "#conversation-#{conversation2.id}"
          end
        end

        context 'when conversations without Last Contact not present' do
          before { conversation2 }

          let(:conversation2) { create :conversation, person_id: loan_officer.id, organization: organization }

          it 'displays nothing' do
            expect(response.body).to have_selector "[data-conversations-count='0']"
          end
        end

        context 'when another organization has conversations without Last Contact and the current organization does not' do
          before { message }

          let(:person) { create :person }
          let!(:organization2) { create :organization }
          let(:conversation2) { create :conversation, exchanges: 0, person: person, organization: organization2 }
          let(:message) { create :message, conversation: conversation2, author: person, organization: organization2 }

          it 'does not display conversations belonging another organization' do
            expect(response.body).not_to have_selector "#conversation-#{conversation2.id}"
          end
        end
      end

      context 'when displays search input' do
        it { expect(response.body).to have_selector '#search' }
      end

      context 'when displays search button' do
        it { expect(response.body).to have_selector '#search_button' }
      end

      context 'when sees the search input' do
        let(:response) { get :index, params: { search: search_query } }

        context 'when enters text value in search input and click search button' do
          let(:search_query) { 'User Name' }

          context 'when person name includes that value' do
            before { message }

            let(:person) { create :person, name: search_query }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
            let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization }

            it 'displays conversations with that person' do
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end

          context 'when more of one person name includes that value' do
            before do
              message1
              message2
            end

            let(:person1) { create :person, name: search_query }
            let(:person2) { create :person, name: "#{search_query}2" }
            let(:conversation1) { create :conversation, exchanges: 0, person: person1, organization: organization }
            let(:conversation2) { create :conversation, exchanges: 0, person: person2, organization: organization }
            let(:message1) { create :message, conversation: conversation1, author_type: Person.to_s, author: person1, organization: organization }
            let(:message2) { create :message, conversation: conversation2, author_type: Person.to_s, author: person2, organization: organization }

            it 'displays conversation1' do
              expect(response.body).to have_selector "#conversation-#{conversation1.id}"
            end

            it 'displays conversation2' do
              expect(response.body).to have_selector "#conversation-#{conversation2.id}"
            end
          end

          context 'when person name does not include search value' do
            let(:search_query) { 'No_User_Name' }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end

          context 'when another organization has conversations with person name with that value and the current organization does not' do
            before { message }

            let(:person) { create :person, name: search_query }
            let(:organization1) { create :organization }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization1 }
            let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization1 }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end
        end

        context 'when enters empty value and click search button' do
          let(:search_query) { '' }

          it 'displays all conversations from current organization' do
            conversations.each do |conversation|
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end
        end

        context 'when enters one of special characters _&#$,=+|\?/*^;:%@<>() and click search button' do
          let(:search_query) { '_&#$,=+|\?/*^";:%@<>()' }

          it 'displays nothing' do
            expect(response.body).to have_selector "[data-conversations-count='0']"
          end
        end

        context 'when enters text value with dot(.) in search input and click search button' do
          let(:search_query) { 'Mr. Test User' }

          context 'when person name includes that value' do
            before { message }

            let(:person) { create :person, name: search_query }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
            let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization }

            it 'displays conversations with that person' do
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end

          context 'when person name does not include that value' do
            it 'when person name does not include that value' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end
        end

        context 'when enters text value with apostrophe(’) in search input and click search button' do
          let(:search_query) { 'Mr. Test User' }

          context 'when person name includes that value' do
            before { message }

            let(:person) { create :person, name: search_query }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
            let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization }

            it 'displays conversations with that person' do
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end

          context 'when person name does not include that value' do
            it 'when person name does not include that value' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end
        end

        context 'when enters text value with length more then 100 chars in search input and click search button' do
          let(:search_query) { 'Mr. Test User' * 25 }

          it 'when person name does not include that value' do
            expect(response.body).to have_selector "[data-conversations-count='0']"
          end
        end

        context 'when enters sequence of numbers in search input and click search button' do
          let(:search_query) { '+14150000200' }

          context 'when search person by mobile number' do
            context 'when person mobile includes that sequence of numbers' do
              before { message }

              let(:person) { create :person, mobile: search_query }
              let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
              let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization }

              it 'displays conversations with that person' do
                expect(response.body).to have_selector "#conversation-#{conversation.id}"
              end
            end

            context 'when person mobile does not include that sequence of numbers' do
              it 'displays nothing' do
                expect(response.body).to have_selector "[data-conversations-count='0']"
              end
            end

            context 'when more of one person mobile includes that sequence of numbers' do
              before do
                message1
                message2
              end

              let(:search_query) { '+141500002' }

              let(:person1) { create :person, mobile: "#{search_query}00" }
              let(:person2) { create :person, mobile: "#{search_query}01" }
              let(:conversation1) { create :conversation, exchanges: 0, person: person1, organization: organization }
              let(:conversation2) { create :conversation, exchanges: 0, person: person1, organization: organization }
              let(:message1) { create :message, conversation: conversation1, author_type: Person.to_s, author: person1, organization: organization }
              let(:message2) { create :message, conversation: conversation2, author_type: Person.to_s, author: person2, organization: organization }

              it 'displays conversation1' do
                expect(response.body).to have_selector "#conversation-#{conversation1.id}"
              end

              it 'displays conversation2' do
                expect(response.body).to have_selector "#conversation-#{conversation2.id}"
              end
            end

            context 'when another organization has conversations with person mobile with that sequence of numbers and the current organization does not' do
              before { message }

              let(:person) { create :person, mobile: search_query }
              let(:organization1) { create :organization }
              let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization1 }
              let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization1 }

              it 'displays nothing' do
                expect(response.body).to have_selector "[data-conversations-count='0']"
              end
            end
          end
        end

        context 'when enters loan officer name in search input and click search button' do
          let(:search_query) { 'Loan Officer' }

          context 'when conversations last contact includes that loan officer name' do
            before { message }

            let(:person) { create :person, name: search_query }
            let(:loan_officer) { create :loan_officer, organization: organization, name: search_query }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
            let(:message) { create :message, conversation: conversation, author_type: LoanOfficer.to_s, author: loan_officer, organization: organization }

            it 'displays conversations with that value' do
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end

          context 'when conversations last contact does not include that loan officer name' do
            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end

          context 'when another organization has conversations with that loan officer name and the current organization does not' do
            before { message }

            let(:person) { create :person, name: search_query }
            let(:organization1) { create :organization }
            let(:officer_mcclain) { create :officer_mcclain, organization: organization1, name: search_query }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization1 }
            let(:message) { create :message, conversation: conversation, author_type: LoanOfficer.to_s, author: officer_mcclain, organization: organization1 }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end
        end

        context 'when enters city in search input and click search button' do
          let(:search_query) { 'Paris' }

          context 'when person city includes that value' do
            before { message }

            let(:person) { create :person, city: search_query }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization }
            let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization }

            it 'displays conversations with that person' do
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end

          context 'when more of one person city includes that value' do
            before do
              message1
              message2
            end

            let(:person1) { create :person, city: search_query }
            let(:person2) { create :person, city: search_query }
            let(:conversation1) { create :conversation, exchanges: 0, person: person1, organization: organization }
            let(:conversation2) { create :conversation, exchanges: 0, person: person2, organization: organization }
            let(:message1) { create :message, conversation: conversation1, author_type: Person.to_s, author: person1, organization: organization }
            let(:message2) { create :message, conversation: conversation2, author_type: Person.to_s, author: person2, organization: organization }

            it 'displays conversations1' do
              expect(response.body).to have_selector "#conversation-#{conversation1.id}"
            end

            it 'displays conversation2' do
              expect(response.body).to have_selector "#conversation-#{conversation2.id}"
            end
          end

          context 'when person city does not include search value' do
            let(:search_query) { 'No_City_Name' }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end

          context 'when another organization has conversations with person city with that value and the current organization does not' do
            before { message }

            let(:person) { create :person, city: search_query }
            let(:organization1) { create :organization }
            let(:conversation) { create :conversation, exchanges: 0, person: person, organization: organization1 }
            let(:message) { create :message, conversation: conversation, author_type: Person.to_s, author: person, organization: organization1 }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end
        end

        context 'when enters city in search input and click search button' do
          let(:search_query) { 'Topic Name' }

          context 'when conversations topic name includes that topic name' do
            before { conversation }

            let(:conversation) { create :conversation, organization: organization, topic_name: search_query }

            it 'displays conversations with that topic name' do
              expect(response.body).to have_selector "#conversation-#{conversation.id}"
            end
          end

          context 'when conversations topic name does not include that topic name' do
            let(:search_query) { 'No_Topic_Name' }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end

          context 'when another organization has conversations with that topic name and the current organization does not' do
            before { conversation }

            let(:search_query) { 'No_Topic_Name' }
            let(:organization1) { create :organization }
            let(:conversation) { create :conversation, organization: organization1, topic_name: search_query }

            it 'displays nothing' do
              expect(response.body).to have_selector "[data-conversations-count='0']"
            end
          end
        end
      end
    end

    context 'when conversations not present' do
      before do
        sign_in(loan_officer2)
        get(:index)
      end

      let(:organization1) { create :organization }
      let(:loan_officer2) { create :loan_officer, organization: organization1 }

      it 'displays nothing' do
        expect(response.body).to have_selector "[data-conversations-count='0']"
      end
    end
  end
end
