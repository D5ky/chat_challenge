# frozen_string_literal: true

require 'rails_helper'

describe Conversation do
  context 'with valid organization and user' do
    subject { build :conversation }

    it { is_expected.to be_valid }
  end

  context 'without valid organization' do
    subject { build :conversation, organization: nil }

    it { is_expected.to be_invalid }
  end

  context 'without valid person' do
    subject { build :conversation, person: nil }

    it { is_expected.to be_invalid }
  end

  describe '#messages' do
    subject(:messages) { conversation.messages }

    let(:organization) { create :organization }
    let(:conversation) { create :conversation, organization: organization }

    context 'when owning messages' do
      let!(:message) { create :message, organization: organization, conversation: conversation }

      it 'returns an array of messages' do
        expect(messages).to include message
      end
    end

    context 'when not having any messages' do
      it { is_expected.to be_empty }
    end
  end

  describe '#last_message' do
    subject(:last_message) { conversation.last_message }

    let(:organization) { create :organization }
    let(:loan_officer) { create :loan_officer, organization: organization }

    context 'when owning messages' do
      let(:conversation) { create :conversation, organization: organization, loan_officer: loan_officer, exchanges: 2 }

      it 'returns an array of messages' do
        expect(last_message).to eq Message.last
      end
    end

    context 'when not having any messages' do
      let(:conversation) { create :conversation, organization: organization }

      it { is_expected.to be_nil }
    end
  end

  describe '#last_message_from_officer' do
    subject(:last_message_from_officer) { conversation.last_message_from_officer }

    let(:organization) { create :organization }
    let(:loan_officer) { create :loan_officer, organization: organization }

    context 'when owning messages from officer' do
      let(:conversation) { create :conversation, organization: organization, loan_officer: loan_officer, exchanges: 2 }

      it 'returns an array of messages' do
        expect(last_message_from_officer).to eq Message.last_from_officer
      end
    end

    context 'when not having any messages' do
      let(:conversation) { create :conversation, organization: organization }

      it { is_expected.to be_nil }
    end
  end

  describe '#last_message_body' do
    subject(:last_message_body) { conversation.last_message_body }

    let(:organization) { create :organization }
    let(:loan_officer) { create :loan_officer, organization: organization }

    context 'when owning messages' do
      let(:conversation) { create :conversation, organization: organization, loan_officer: loan_officer, exchanges: 2 }

      it 'returns an array of messages' do
        expect(last_message_body).to eq Message.last.body
      end
    end

    context 'when not having any messages' do
      let(:conversation) { create :conversation, organization: organization }

      it { is_expected.to be_nil }
    end
  end

  describe '#last_contact' do
    subject(:last_contact) { conversation.last_contact }

    let(:organization) { create :organization }
    let(:loan_officer) { create :loan_officer, organization: organization }

    context 'when a loan_officer contacted with conversation' do
      let(:conversation) { create :conversation, organization: organization, loan_officer: loan_officer, exchanges: 2 }

      it 'returns the last agent that contacted with the conversation' do
        expect(last_contact).to eq loan_officer
      end
    end

    context 'when multiple loan_officers contacted with conversation' do
      let(:officer_mcclain) { create :officer_mcclain, organization: organization }
      let(:conversation)    { create :conversation, organization: organization }
      let(:messages) do
        create_list(:message, 2, author: loan_officer, organization: organization, conversation: conversation) +
          create_list(:message, 2, author: officer_mcclain, organization: organization, conversation: conversation)
      end

      it 'returns the last agent that contacted with the conversation' do
        messages
        expect(last_contact).to eq officer_mcclain
      end
    end

    context 'when no one contacted with conversation' do
      let(:conversation) { create :conversation, organization: organization }

      it 'returns the last agent that contacted with the conversation' do
        expect(last_contact).to be_nil
      end
    end
  end

  describe '#last_contact_name' do
    subject(:last_contact_name) { conversation.last_contact_name }

    let(:organization) { create :organization }
    let(:loan_officer) { create :loan_officer, organization: organization }

    context 'when a loan_officer contacted with conversation' do
      let(:conversation) { create :conversation, organization: organization, loan_officer: loan_officer, exchanges: 2 }

      it 'returns the last agent that contacted with the conversation' do
        expect(last_contact_name).to eq loan_officer.name
      end
    end

    context 'when multiple loan_officers contacted with conversation' do
      let(:officer_mcclain) { create :officer_mcclain, organization: organization }
      let(:conversation)    { create :conversation, organization: organization }
      let(:messages) do
        create_list(:message, 2, author: loan_officer, organization: organization, conversation: conversation) +
          create_list(:message, 2, author: officer_mcclain, organization: organization, conversation: conversation)
      end

      it 'returns the last agent that contacted with the conversation' do
        messages
        expect(last_contact_name).to eq officer_mcclain.name
      end
    end

    context 'when no one contacted with conversation' do
      let(:conversation) { create :conversation, organization: organization }

      it 'returns the last agent that contacted with the conversation' do
        expect(last_contact_name).to be_nil
      end
    end
  end
end
