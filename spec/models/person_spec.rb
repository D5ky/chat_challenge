# frozen_string_literal: true

require 'rails_helper'

describe Person, type: :model do
  context 'with valid attributes' do
    subject { create :person }

    it { is_expected.to be_valid }
  end

  context 'with invalid attributes' do
    it { is_expected.to be_invalid }
  end
end
