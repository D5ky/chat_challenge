# frozen_string_literal: true

require 'rails_helper'

describe Organization do
  describe 'validity' do
    context 'with valid attributes' do
      subject { create :organization }

      it { is_expected.to be_valid }
    end

    context 'with invalid attributes' do
      it { is_expected.to be_invalid }
    end
  end

  describe '#messages' do
    subject(:messages) { organization.messages }

    let(:organization) { create :organization }

    context 'when owning messages' do
      let!(:message) { create :message, organization: organization }

      it 'returns an array of messages' do
        expect(messages).to include message
      end
    end

    context 'when not having any messages' do
      it { is_expected.to be_empty }
    end
  end

  describe '#loan_officers' do
    subject(:loan_officers) { organization.loan_officers }

    let(:organization) { create :organization }

    context 'when owning loan_officers' do
      let!(:loan_officer) { create :loan_officer, organization: organization }

      it 'returns an array of loan_officers' do
        expect(loan_officers).to include loan_officer
      end
    end

    context 'when not having any officers' do
      it { is_expected.to be_empty }
    end
  end
end
