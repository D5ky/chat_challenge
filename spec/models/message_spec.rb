# frozen_string_literal: true

require 'rails_helper'

describe Message do
  context 'with valid attributes' do
    subject { create :message }

    it { is_expected.to be_valid }
  end

  context 'without valid attributes' do
    subject(:message) { described_class.new }

    before { message.valid? }

    it { is_expected.to be_invalid }
    it { expect(message.errors[:body]).to include "can't be blank" }
    it { expect(message.errors[:conversation]).to include 'must exist' }
    it { expect(message.errors[:organization]).to include 'must exist' }
  end

  describe '.from_officer' do
    subject(:subject) { described_class.from_officer }

    context 'when messages from loan officers exist' do
      let!(:organization) { create :organization }
      let!(:loan_officer) { create :loan_officer, organization: organization }
      let!(:messages)     { create_list :message, 2, organization: organization, author: loan_officer }

      it('returns messages created by the loan officer') { is_expected.to match_array messages }
    end

    context 'when messages from people exist' do
      let!(:organization) { create :organization }
      let!(:person)       { create :person }
      let!(:messages)     { create_list :message, 2, organization: organization, author: person }

      it { is_expected.to be_empty }
      it('does not return messages from other person') { is_expected.not_to match_array messages }
    end

    context 'when no messages exist' do
      it { is_expected.to be_empty }
    end
  end
end
