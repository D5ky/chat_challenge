# frozen_string_literal: true

class Person < ApplicationRecord
  # Associations
  # ----------------

  has_many :conversations, dependent: :destroy

  # Validations
  # -----------

  validates :name, presence: true
  validates :city, presence: true
  validates :mobile, presence: true, format: { with: sanitized_mobile_regex }
end
