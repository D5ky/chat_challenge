# frozen_string_literal: true

class Message < ApplicationRecord
  # Associations
  # --------------

  belongs_to :organization
  belongs_to :conversation
  belongs_to :author, polymorphic: true

  # Validations
  # --------------

  validates :body, presence: true

  # Scopes
  # --------------

  scope :from_officer, -> { where(author_type: LoanOfficer.to_s) }
  scope :last_from_officer, -> { from_officer.last }

  # Callbacks
  # --------------

  after_create :update_conversation_last_message_at

  private

  def update_conversation_last_message_at
    conversation.update(last_message_at: Time.current)
  end
end
