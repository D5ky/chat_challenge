# frozen_string_literal: true

class Organization < ApplicationRecord
  # Associations
  # ----------------

  has_many :messages, dependent: :destroy
  has_many :conversations, dependent: :destroy
  has_many :loan_officers, dependent: :destroy

  # Validations
  # ----------------

  validates :name, presence: true
end
