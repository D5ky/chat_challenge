# frozen_string_literal: true

class ConversationsController < ApplicationController
  def index
    @conversations = conversations.paginate(page: params[:page], per_page: params[:per_page])
  end

  def show; end

  private

  def conversations
    return search_conversations if params[:search]

    return conversation_query.without_last_contact if params[:without_last_contact]

    conversation_query.order_last_massage_at_desc
  end

  def conversation_query
    ConversationQuery.new(current_organization.conversations)
  end

  def search_conversations
    Search.new(current_organization, params[:search]).call
  end
end
