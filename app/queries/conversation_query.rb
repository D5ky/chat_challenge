# frozen_string_literal: true

class ConversationQuery
  attr_reader :relation

  def initialize(relation = Conversation.all)
    @relation = relation
  end

  def order_last_massage_at_desc
    relation.order(last_message_at: :desc)
  end

  def without_last_contact
    relation
      .where(id: only_person_messages_ids)
      .order(last_message_at: :desc)
  end

  def joins_persons
    relation.joins(:person)
  end

  def find_by_topic_name(topic_name)
    relation.where('topic_name like ?', "%#{topic_name}%")
  end

  def find_by_person_name(name)
    joins_persons.where('people.name like ?', "%#{name}%")
  end

  def find_by_person_mobile(number)
    joins_persons.where('people.mobile like ?', "%#{number}%")
  end

  def find_by_person_city(city)
    joins_persons.where('people.city like ?', "%#{city}%")
  end

  def find_by_last_contact(organization_id, name)
    sql = "INNER JOIN messages ON messages.conversation_id = conversations.id
             AND messages.author_type = 'LoanOfficer'
           LEFT JOIN loan_officers ON messages.author_id = loan_officers.id
             AND strftime('%Y-%m-%d %H:%M', conversations.last_message_at) = strftime('%Y-%m-%d %H:%M', messages.created_at)
           WHERE loan_officers.name like '%#{name}%'
           AND conversations.organization_id = #{organization_id}"
    Conversation.joins(sql)
  end

  delegate :none, to: :joins_persons

  private

  def uniq_messages_ids_from_loan_officers
    Message.where(author_type: LoanOfficer.to_s).pluck(:conversation_id).uniq
  end

  def all_messages_ids
    Message.pluck(:conversation_id)
  end

  def only_person_messages_ids
    all_messages_ids - uniq_messages_ids_from_loan_officers
  end
end
