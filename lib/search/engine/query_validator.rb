# frozen_string_literal: true

module Engine
  class QueryValidator
    MAX_QUERY_LENGTH         = 100
    SPECIAL_CHARACTERS_REGEX = %r{[\!\"\#\$\%\&\(\)\*\+\,\-\/\:\;\<\>\=\?\@\[\]\{\}\\\\\^\_\`\~]+$}.freeze

    def initialize(query)
      @query = query
    end

    def call
      query_is_valid?
    end

    private

    attr_reader :query

    def query_is_valid?
      return if special_characters_present? || query_length_invalid?

      true
    end

    def special_characters_present?
      query[SPECIAL_CHARACTERS_REGEX]
    end

    def query_length_invalid?
      query.length > MAX_QUERY_LENGTH
    end
  end
end
