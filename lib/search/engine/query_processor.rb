# frozen_string_literal: true

module Engine
  class QueryProcessor
    def initialize(organization, query)
      @organization = organization
      @query        = query
    end

    def call
      process
    end

    private

    attr_reader :organization, :query

    def process
      [
        find_by_person_name.or(find_by_person_mobile).or(find_by_person_city),
        find_by_last_contact,
        find_by_topic_name
      ].sum
    end

    def find_by_person_name
      conversations_query.find_by_person_name(query)
    end

    def find_by_person_mobile
      number = query[/\d+/]

      return empty_relation unless number

      conversations_query.find_by_person_mobile(number)
    end

    def find_by_last_contact
      conversations_query.find_by_last_contact(organization.id, query)
    end

    def find_by_person_city
      conversations_query.find_by_person_city(query)
    end

    def find_by_topic_name
      conversations_query.find_by_topic_name(query)
    end

    def conversations_query
      ConversationQuery.new(organization.conversations)
    end

    def empty_relation
      ConversationQuery.new.none
    end
  end
end
