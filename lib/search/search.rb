# frozen_string_literal: true

class Search
  def initialize(organization, query)
    @organization = organization
    @query        = query
  end

  def call
    return empty_relation unless query_is_valid?

    search_results
  end

  private

  attr_reader :organization, :query

  def query_is_valid?
    Engine::QueryValidator.new(query).call
  end

  def search_results
    Engine::QueryProcessor.new(organization, query).call
  end

  def empty_relation
    ConversationQuery.new.none
  end
end
