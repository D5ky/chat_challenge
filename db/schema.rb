# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_14_155855) do

  create_table "conversations", force: :cascade do |t|
    t.integer "organization_id"
    t.integer "person_id"
    t.datetime "last_message_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "topic_name"
    t.index ["organization_id"], name: "index_conversations_on_organization_id"
    t.index ["person_id"], name: "index_conversations_on_person_id"
  end

  create_table "loan_officers", force: :cascade do |t|
    t.string "name"
    t.integer "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_loan_officers_on_email", unique: true
    t.index ["organization_id"], name: "index_loan_officers_on_organization_id"
    t.index ["reset_password_token"], name: "index_loan_officers_on_reset_password_token", unique: true
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.integer "organization_id"
    t.integer "conversation_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_messages_on_author_type_and_author_id"
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["organization_id"], name: "index_messages_on_organization_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.string "mobile"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "city"
  end

end
