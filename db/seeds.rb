# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# require 'factory_bot_rails'

organization    = FactoryBot.create :organization
loan_officer    = FactoryBot.create :loan_officer, organization: organization
officer_mcclain = FactoryBot.create :officer_mcclain, organization: organization
FactoryBot.create_list :conversation, 100, organization: organization, loan_officer: loan_officer, exchanges: 2
FactoryBot.create_list :conversation, 100, organization: organization, loan_officer: officer_mcclain, exchanges: 2
