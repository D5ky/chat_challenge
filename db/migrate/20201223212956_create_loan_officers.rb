class CreateLoanOfficers < ActiveRecord::Migration[5.2]
  def change
    create_table :loan_officers do |t|
      t.string :name
      t.references :organization, foreign_key: true

      t.timestamps
    end
  end
end
