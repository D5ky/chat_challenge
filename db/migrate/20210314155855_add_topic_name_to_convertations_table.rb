class AddTopicNameToConvertationsTable < ActiveRecord::Migration[5.2]
  def up
    add_column :conversations, :topic_name, :string
  end

  def down
    remove_column :conversations, :topic_name, :string
  end
end
