class AddCityToPeopleTable < ActiveRecord::Migration[5.2]
  def up
    add_column :people, :city, :string
  end

  def down
    remove_column :people, :city, :string
  end
end
